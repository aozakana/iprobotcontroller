import time
import can

def python_can_test():
	bus = can.interface.Bus(  channel='can0', bustype='socketcan_native', bitrate=500000, canfilters=None)
	try:
		while True:
			msg = bus.recv(100)
			if( msg):
				print(msg)
	except KeyboardInterrupt:
		print('exit script')
	
if __name__ =='__main__':
	python_can_test()
