import pygame.mixer
import time

def music_test():
	pygame.mixer.init()
	pygame.mixer.music.load('n99.mp3')
	pygame.mixer.music.set_volume(0.1)	
	pygame.mixer.music.play(-1)
	
	try:
		while True:
			time.sleep(0.0001)
	except KeyboardInterrupt:
		pygame.mixer.music.stop()
	
if __name__ =='__main__':
	music_test()
