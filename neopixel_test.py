import time
from rpi_ws281x import *

def neopixel_test():
	LED_COUNT      = 12      # Number of LED pixels.
	LED_PIN        = 13      # GPIO pin connected to the pixels (must support PWM!).
	LED_FREQ_HZ    = 800000  # LED signal frequency in hertz (usually 800khz)
	LED_DMA        = 10      # DMA channel to use for generating signal (try 10)
	LED_BRIGHTNESS = 31      # Set to 0 for darkest and 255 for brightest
	LED_INVERT     = False   # True to invert the signal (when using NPN transistor level shift)
	LED_CHANNEL    = 1
	LED_STRIP      = ws.SK6812_STRIP
	
	strip = Adafruit_NeoPixel(LED_COUNT, LED_PIN, LED_FREQ_HZ, LED_DMA, LED_INVERT, LED_BRIGHTNESS, LED_CHANNEL, LED_STRIP)
	strip.begin()

	try:
		while True:
			for i in range(strip.numPixels()):
				strip.setPixelColor(i, Color(255,0,0))		
			strip.show()
			time.sleep(1)
			for i in range(strip.numPixels()):
				strip.setPixelColor(i, Color(0,255,0))		
			strip.show()
			time.sleep(1)
			for i in range(strip.numPixels()):
				strip.setPixelColor(i, Color(0,0,255))		
			strip.show()
			time.sleep(1)
	except KeyboardInterrupt:
		for i in range(strip.numPixels()):
			strip.setPixelColor(i, Color(0,0,0))		
		strip.show()
if __name__ =='__main__':
	neopixel_test()
