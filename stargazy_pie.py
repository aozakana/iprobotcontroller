import time
import rpi_ws281x
import can


def stargazy_pie():
	bus = can.interface.Bus(  channel='can0', bustype='socketcan_native', bitrate=500000, canfilters=None)	
	
	try:
		# enable control
		txmsg = can.Message(arbitration_id=0x103, is_extended_id = False, dlc=1, data=[0x01])
		bus.send(txmsg)
		time.sleep(0.004)
		while True:
			rxmsg = bus.recv(100)
			if( rxmsg):
				print(rxmsg)			
	except KeyboardInterrupt:
		# disable control
		txmsg = can.Message(arbitration_id=0x103, is_extended_id = False, dlc=1, data=[0x00])
		bus.send(txmsg)
		time.sleep(0.004)		
		print('exit script')
	
if __name__ =='__main__':
	stargazy_pie()
