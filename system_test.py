import sys
import time
import threading
from multiprocessing import Value
import can
import pygame.mixer
from rpi_ws281x import *

def neopixel_test():
	# initialize LED
	strip = Adafruit_NeoPixel(12, 13, 800000, 10, False, 15, 1, ws.SK6812_STRIP)
	strip.begin()
	led_color = [ Color(0,0,0), Color(255,0,0), Color(0,255,0), Color(255,255,0), Color(0,0,255), Color(255,0,255), Color(0,255,255), Color(255,255,255) ]

	while True:
		if( exit_flag == 0 ):
			for c in led_color:
				for i in range(strip.numPixels()):
					strip.setPixelColor(i, c)		
				strip.show()
				time.sleep(0.5)
		else:
			# turn off LED
			for i in range(strip.numPixels()):
				strip.setPixelColor(i, Color(0,0,0))		
			strip.show()
			print('LED stop')
			return

def speaker_test():
	# initialize Speaker
	pygame.mixer.init()
	pygame.mixer.music.load('n99.mp3')
	pygame.mixer.music.set_volume(0.1)	
	pygame.mixer.music.play(-1)
	while True:
		if( exit_flag == 0 ):			
			time.sleep(0.001)
		else:
			# stop music
			pygame.mixer.music.stop()
			print('Speaker stop')
			return
	
def can_test():
	# initialize CAN bus
	bus = can.interface.Bus(  channel='can0', bustype='socketcan_native', bitrate=500000, canfilters=None)		
	while True:
		if( exit_flag == 0 ):			
			msg = bus.recv(10)
			if( msg):
				print(msg)
		else:
			# stop CAN bus ( nothing to do )
			print('CAN bus stop')
			return

def system_test():	
	global exit_flag
	
	thread_neopixel = threading.Thread(target=neopixel_test)
	thread_speaker = threading.Thread(target=speaker_test)
	thread_can = threading.Thread(target=can_test)
	exit_flag = 0
	
	try:
		thread_neopixel.start()
		thread_speaker.start()
		thread_can.start()
		
		while True:
			time.sleep(1)		
	except KeyboardInterrupt:
		exit_flag = 1
		time.sleep(0.5)
		print('system test stop')
		sys.exit()

if __name__ =='__main__':
	system_test()
